package ejercicio7;

import java.util.Arrays;

public class Ejercicio4 {

    public static void main(String[] args) {

        int array[] = new int[10];
        int numPares = 0, numImpares = 0;
      

        for (int i = 0; i < array.length; i++) {
            array[i] = i+1;
        }
        
        for (int i = 0; i < array.length; i++) {
            if ((i+1) % 2 == 0) {
                array[i]=0;
            }
        }
        
        for (int i = 0; i < array.length; i++) {
			if (array[i] == 0) {
				numPares++;
			} else {
				numImpares++;
			}
		}

        System.out.println(Arrays.toString(array));
        System.out.println("Ceros: " + numPares + ". Distintos de cero: " + numImpares + ".");

    }

}

